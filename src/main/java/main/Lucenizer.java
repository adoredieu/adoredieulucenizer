package main;

import org.apache.lucene.store.SimpleFSDirectory;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import main.dao.VerseDao;
import main.lucene.Indexer;
import org.lukhnos.portmobile.file.Paths;

public class Lucenizer
{
    public static void execute(File dbFile,
                               File outputDir) throws IOException, SQLException
    {
        Indexer indexer = new Indexer(new VerseDao(dbFile));
        indexer.index(new SimpleFSDirectory(Paths.get(outputDir.getPath())));
    }
}
