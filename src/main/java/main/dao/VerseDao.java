package main.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import main.model.Verse;

public class VerseDao
{
    private File dbFile;

    public VerseDao(File dbFile)
    {
        this.dbFile = dbFile;
    }

    private void disconnect(Connection connection)
    {
        try
        {
            connection.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private Connection connect(File dbFile)
    {
        Connection connection = null;

        System.out.println("Opening database...");
        try
        {
            // load the sqlite-JDBC driver using the current class loader
            Class.forName("org.sqlite.JDBC");

            connection = DriverManager.getConnection("jdbc:sqlite:" + dbFile.getPath());
            connection.setAutoCommit(false);

            System.out.println("Opened database successfully");
        }
        catch (Exception e)
        {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        return connection;
    }

    public List<Verse> getAllVerses() throws SQLException
    {
        List<Verse> verses = new ArrayList<Verse>();
        Connection connection = connect(dbFile);

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM " + Verse.BIBLE_TABLE + ";");
        while (rs.next())
        {
            int id = rs.getInt(Verse.VERSE_ID_COLUMN);
            String text = rs.getString(Verse.VERSE_TEXT_COLUMN);

            Verse verse = new Verse();
            verse.setId(id);
            verse.setText(text);
            verses.add(verse);
        }

        rs.close();
        stmt.close();

        disconnect(connection);

        return verses;
    }
}
