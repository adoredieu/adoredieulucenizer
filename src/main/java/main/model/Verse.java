package main.model;

public class Verse
{
    public static final String BIBLE_TABLE = "bible";
    public static final String VERSE_TEXT_COLUMN = "text";
    public static final String VERSE_ID_COLUMN = "_id";

    private Integer _id;
    private String text;

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public Integer getId()
    {
        return _id;
    }

    public void setId(Integer id)
    {
        this._id = id;
    }
}
