package main.lucene;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.FieldInfo;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import main.dao.VerseDao;
import main.model.Verse;

public class Indexer
{
    private VerseDao verseDao;

    public Indexer(VerseDao verseDao)
    {
        this.verseDao = verseDao;
    }

    private void addVerseToDoc(IndexWriter indexWriter,
                               int id,
                               String text) throws IOException
    {
        Document doc = new Document();

        doc.add(new IntField("id", id, Field.Store.YES));
        doc.add(new TextField("text", text, Field.Store.YES));

        // Create a field with term vector enabled
        FieldType type = new FieldType();
        //type.setIndexed(true);
        type.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
        type.setStored(true);
        type.setStoreTermVectors(true);
        type.setTokenized(true);
        type.setStoreTermVectorOffsets(true);
        Field field = new Field("text_term", text, type); //with term vector enabled
        doc.add(field);

        indexWriter.addDocument(doc);
    }

    public void index(Directory outpoutPath) throws IOException, SQLException
    {
        IndexWriterConfig config = new IndexWriterConfig(AnalyzerFactory.getDefaultAnalyzer());
        IndexWriter indexWriter = new IndexWriter(outpoutPath, config);
        indexWriter.deleteAll();

        System.out.println("Getting all verses...");
        List<Verse> allVerses = verseDao.getAllVerses();
        System.out.println("Get all verses successfully");

        System.out.println("Creating index...");
        for (int i = 0; i < allVerses.size(); i++)
        {
            try
            {
                addVerseToDoc(indexWriter, allVerses.get(i).getId(), allVerses.get(i).getText());
                System.out.println("Adding " + i + "/" + allVerses.size());
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        indexWriter.commit();
        indexWriter.close();
        System.out.println("Created index successfully");
    }
}
