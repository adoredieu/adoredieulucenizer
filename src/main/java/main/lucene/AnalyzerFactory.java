package main.lucene;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.lucene.util.Version;

public class AnalyzerFactory
{
    public static Analyzer getDefaultAnalyzer()
    {
        return new FrenchAnalyzer();
    }

    public static Version getLuceneVersion()
    {
        return Version.LUCENE_5_3_0;
    }
}
